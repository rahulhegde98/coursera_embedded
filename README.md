/* Add Author and Project Details here */

Author Details
**************************************************************************************************
Author Name:Rahul Hegde
Date: 26.04.2020
**************************************************************************************************

Project Description
**************************************************************************************************
The "stats.c" file consists of the main body of the code with the function calls and the function definitions. The "stats.h" file contains the function declarations of the functions used in the main programme.

The main() block contains seven functions out of which four functions namely: find_mean(), find_median(), find_maximum(), find_minimum() obtain the mean, median, minimum and maximum values for a given array. The function print_array() prints the array given as an input to it. The function print_statistics() prints the mean, median, minimum, maximum values obtained. The function sort_array() sorts an array in the descending order. 

In order to obtain the median the array must be sorted either in the ascending or descending order else the output will produce erraneous results.

************************************************************************************************** 
