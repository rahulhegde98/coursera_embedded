/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief The "stats.h" file consists of function declarations of functions that find the mean, median, maximum
   minimum number in a given array. It also contains function declarations of functions that print and sort an array in descending order. 
 *
 * //The find_minimum function calculates the minimum value of the given array
//The find_maximum function calculates the maximum value of the given array
//The find_mean function calculates the mean value of the given array
//The find_median function calculates the median value of the given array
//The sort_array function sorts the given array in desceding order.
//The print_array function prints an array
 *
 * @author Rahul Hegde
 * @date 26.04.2020
  modified on 02.05.2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */

//The find_minimum function calculates the minimum value of the given array
//The find_maximum function calculates the maximum value of the given array
//The find_mean function calculates the mean value of the given array
//The find_median function calculates the median value of the given array
//The sort_array function sorts the given array in desceding order.
//The print_array function prints an array
//The print_statistics function prints the statistics values such as maximum, minimum,mean, median
 
char find_minimum(unsigned char *test, int array_size);
char find_maximum(unsigned char *test, int array_size);
char find_mean(unsigned char *test, int array_size);
char *find_median(unsigned char *test, int array_size, unsigned char *temp);
char *sort_array(unsigned char *test, int array_size);
char print_array(unsigned char *test, int array_size);
char print_statistics(unsigned char *minimum_number, unsigned char *maximum_number, unsigned char *mean, unsigned char *median, int array_size);
 

/**
 * @brief find_minimum() fuction calculates the minimum value of the given array
 *
 * find_minimum() accepts a unsigned character pointer to the test array and interger size of the array as its arguements and returns a minimum number of character datatype from the array
 *
 * @param test <unsigned character pointer for the test array>
 * @param array_size <integer value of size of the array>
 *
 * @return < returns the minimum number as a character datatype>
 */

/**
 * @brief find_maximum() fuction calculates the maximum value of the given array
 *
 * find_maximum() accepts a unsigned character pointer to the test array and interger size of the array as its arguements and returns a maximum number of character datatype array
 *
 * @param test <unsigned character pointer for the test array>
 * @param array_size <integer value of size of the array>
 *
 * @return < returns the maximum number as a character datatype>
 */

/**
 * @brief find_mean() fuction calculates the mean of the values of the given array
 *
 * find_mean() accepts a unsigned character pointer to the test array and interger size of the array as its arguements and returns the mean value of the given array of values.
 *
 * @param test <unsigned character pointer for the test array>
 * @param array_size <integer value of size of the array>
 *
 * @return < returns the mean as a character datatype>
 */

/**
 * @brief find_median() fuction calculates the median of the values in the given array.
 *
 * find_median() accepts a unsigned character pointer to the test array, an interger size of the array and a unsigned character pointer as its arguements and returns a minimum number of character datatype. The array to this function must be sorted in either ascending or descending order.
 *
 * @param test <unsigned character pointer for the test array>
 * @param array_size <integer value of size of the array>
 * @param temp <an unsigned array pointer that stores the median values of the array>
 * @return < returns the median as an array of character datatype>
 */

/**
 * @brief sort_array() fuction sorts the given array in the descending order
 *
 * sort_array() functions accepts a pointer to an array and sorts the array in the descending orderand returns the sorted array.
 *
 * @param test <unsigned character pointer for the test array>
 * @param array_size <integer value of size of the array>
 * @return < returns the sorted array>
 */

/**
 * @brief print_array() fuction prints an array given as input to it
 *
 * print_array() accepts an array and size of the array as input arguements and prints the array
 *
 * @param test <unsigned character pointer for the test array>
 * @param array_size <integer value of size of the array>
 * @return < returns zero>
 */

/**
 * @brief print_statistics() fuction prints the statistics of a given array
 *
 * print_array() accepts a maximum value, minimum value, mean value, median value of an array and size of the array; and prints it 
 *
 * @param  minimum_number <unsigned character pointer for minimum_number>
 * @param  maximum_number <unsigned character pointer for maximum_number>
 * @param  mean <unsigned character pointer for the mean>
 * @param  median <unsigned character pointer for the median>
 * @param array_size <integer value of size of the array>
 * @return < returns zero>
 */

#endif /* __STATS_H__ */
