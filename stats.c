/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief "stats.c" contains the main body of the program. It also contains the 
    function definitions and the functions calls to calculate the mean, maximum,
    minimum of a given array. It also includes functions to sort an array in descending order
    and print an array.
 *
 * The main body of "stats.c" functions contains function calls to calculate the mean, maximum,
    minimum of a given array using functions find_mean(), find_median(), find_maximum() and  find_minimum() respectively. These functions accept a poniter to character array and the size of the array and returns the mean, median, maximum and minimum of the array respectively. The print array functions accepts a pointer to the array and the size of the array and prints the array. The sort_array() function sorts a given array with its size to an array in descending order.
 *
 * @author Rahul Hegde
 * @date 26.04.2020
  modified on 02.05.2020
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  unsigned char minimum_number;
  unsigned char maximum_number;
  unsigned char mean;
  unsigned char *sorted_array;
  unsigned char *median;
  unsigned char temp[2]={0};

  /* Statistics and Printing Functions Go Here */
  printf("\n The given array is : ");
  print_array(test, SIZE);
  minimum_number = find_minimum(test, SIZE);
  maximum_number = find_maximum(test, SIZE);
  mean = find_mean(test, SIZE);
  sorted_array = sort_array(test, SIZE);
  median = find_median(sorted_array, SIZE, temp);
  print_statistics(&minimum_number, &maximum_number, &mean, median,SIZE);
  printf("\n The sorted array is: ");
  print_array(sorted_array, SIZE);
  printf("\n");

}

char find_minimum(unsigned char *test, int array_size)
{
    unsigned char minimum_num;
    unsigned int i;
    minimum_num = test[0];
    for (i=1;i<array_size;i++)
    {
        if(minimum_num > test[i])
        {
            minimum_num = test[i];
        }
    }
    return minimum_num;
}

char find_maximum(unsigned char *test, int array_size)
{
    unsigned char maximum_num;
    unsigned int i;
    maximum_num = test[0];
    for (i=1;i<array_size;i++)
    {
        if(maximum_num < test[i])
        {
            maximum_num = test[i];
        }
    }
    return maximum_num;
}

char find_mean(unsigned char *test, int array_size)
{
    unsigned int total = 0;
    unsigned int average;
    unsigned int i;
    for(i=0;i<array_size;i++)
    {
        total += test[i];
    }
    average = (char)(total/array_size);
    return average;
}

char *sort_array(unsigned char *test, int array_size)
{
    int i,j;
    char temp;

    for(i=0;i<array_size;i++)
    {
        for(j=0;j<(array_size-1);j++)
        {
            if(test[j]<test[j+1])
            {
                temp = test[j+1];
                test[j+1]=test[j];
                test[j]=temp;
            }
        }
    }
    return test;
}

char print_array(unsigned char *test, int array_size)
{
    int i;
    for(i = 0;i < array_size;i++)
    {
        printf("%d ",test[i]);
    }
    return 0;
}

char *find_median(unsigned char *test, int array_size, unsigned char *temp)
{
    if((array_size%2)==0)
    {
        temp[0]=test[array_size/2];
        temp[1]=test[(array_size/2)-1];
    }
    else
    {
        temp[0]=test[(array_size/2)];
    }
    return temp;
}

char print_statistics(unsigned char *minimum_number, unsigned char *maximum_number, unsigned char *mean, unsigned char *median, int array_size )
{
  printf("\n The minimum number in the given array is %d. \n ", *minimum_number);
  printf("\n The maximum number in the given array is %d. \n ", *maximum_number);
  printf("\n The mean of the given array is is %d. \n ", *mean);
  if(array_size % 2==0)
  {
        printf("\n The median of the given array is %d and %d. \n", median[0], median[1]);
  }
  else
  {
        printf("The median of the given array is %d. \n", median[0]);
  }
	
}
